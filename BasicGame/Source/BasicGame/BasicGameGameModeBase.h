// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BasicGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BASICGAME_API ABasicGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
