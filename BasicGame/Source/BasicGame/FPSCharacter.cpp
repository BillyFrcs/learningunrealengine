// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSCharacter.h"
#include "Engine/World.h"
#include "DamageableActor.h"

// Sets default values
AFPSCharacter::AFPSCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Weapon range 
	this->WeaponRange = 1000.f;
}

// Called when the game starts or when spawned
void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFPSCharacter::FireWeapon()
{
	FHitResult Hit = InstantShot();

	ADamageableActor* HitActor = Cast<ADamageableActor>(Hit.Actor);

	if (HitActor && HitActor->IsAttack)
	{
		HitActor->TakeAttack();
	}
}

FHitResult AFPSCharacter::InstantShot()
{
	FVector RayLocation;
	FRotator RayRotation;
	FVector EndTrace = FVector::ZeroVector;

	APlayerController* const PlayerController = GetWorld()->GetFirstPlayerController();

	if (PlayerController)
	{
		PlayerController->GetPlayerViewPoint(RayLocation, RayRotation);

		EndTrace = RayLocation + (RayRotation.Vector() * this->WeaponRange);
	}

	// FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(InstantShot), true, Instigator);
	
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(InstantShot), true);

	FHitResult Hit(ForceInit);

	GetWorld()->LineTraceSingleByChannel(Hit, RayLocation, EndTrace, ECC_Visibility, TraceParams);

	return Hit;
}

// Called every frame
void AFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

