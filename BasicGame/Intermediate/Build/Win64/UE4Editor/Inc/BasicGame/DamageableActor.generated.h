// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BASICGAME_DamageableActor_generated_h
#error "DamageableActor.generated.h already included, missing '#pragma once' in DamageableActor.h"
#endif
#define BASICGAME_DamageableActor_generated_h

#define BasicGame_Source_BasicGame_DamageableActor_h_12_SPARSE_DATA
#define BasicGame_Source_BasicGame_DamageableActor_h_12_RPC_WRAPPERS
#define BasicGame_Source_BasicGame_DamageableActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define BasicGame_Source_BasicGame_DamageableActor_h_12_EVENT_PARMS
#define BasicGame_Source_BasicGame_DamageableActor_h_12_CALLBACK_WRAPPERS
#define BasicGame_Source_BasicGame_DamageableActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADamageableActor(); \
	friend struct Z_Construct_UClass_ADamageableActor_Statics; \
public: \
	DECLARE_CLASS(ADamageableActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BasicGame"), NO_API) \
	DECLARE_SERIALIZER(ADamageableActor)


#define BasicGame_Source_BasicGame_DamageableActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADamageableActor(); \
	friend struct Z_Construct_UClass_ADamageableActor_Statics; \
public: \
	DECLARE_CLASS(ADamageableActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BasicGame"), NO_API) \
	DECLARE_SERIALIZER(ADamageableActor)


#define BasicGame_Source_BasicGame_DamageableActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADamageableActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADamageableActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamageableActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamageableActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamageableActor(ADamageableActor&&); \
	NO_API ADamageableActor(const ADamageableActor&); \
public:


#define BasicGame_Source_BasicGame_DamageableActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamageableActor(ADamageableActor&&); \
	NO_API ADamageableActor(const ADamageableActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamageableActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamageableActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADamageableActor)


#define BasicGame_Source_BasicGame_DamageableActor_h_12_PRIVATE_PROPERTY_OFFSET
#define BasicGame_Source_BasicGame_DamageableActor_h_9_PROLOG \
	BasicGame_Source_BasicGame_DamageableActor_h_12_EVENT_PARMS


#define BasicGame_Source_BasicGame_DamageableActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BasicGame_Source_BasicGame_DamageableActor_h_12_PRIVATE_PROPERTY_OFFSET \
	BasicGame_Source_BasicGame_DamageableActor_h_12_SPARSE_DATA \
	BasicGame_Source_BasicGame_DamageableActor_h_12_RPC_WRAPPERS \
	BasicGame_Source_BasicGame_DamageableActor_h_12_CALLBACK_WRAPPERS \
	BasicGame_Source_BasicGame_DamageableActor_h_12_INCLASS \
	BasicGame_Source_BasicGame_DamageableActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BasicGame_Source_BasicGame_DamageableActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BasicGame_Source_BasicGame_DamageableActor_h_12_PRIVATE_PROPERTY_OFFSET \
	BasicGame_Source_BasicGame_DamageableActor_h_12_SPARSE_DATA \
	BasicGame_Source_BasicGame_DamageableActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	BasicGame_Source_BasicGame_DamageableActor_h_12_CALLBACK_WRAPPERS \
	BasicGame_Source_BasicGame_DamageableActor_h_12_INCLASS_NO_PURE_DECLS \
	BasicGame_Source_BasicGame_DamageableActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BASICGAME_API UClass* StaticClass<class ADamageableActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BasicGame_Source_BasicGame_DamageableActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
