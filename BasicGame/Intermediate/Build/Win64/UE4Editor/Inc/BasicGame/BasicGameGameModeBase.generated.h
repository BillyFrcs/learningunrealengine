// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BASICGAME_BasicGameGameModeBase_generated_h
#error "BasicGameGameModeBase.generated.h already included, missing '#pragma once' in BasicGameGameModeBase.h"
#endif
#define BASICGAME_BasicGameGameModeBase_generated_h

#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_SPARSE_DATA
#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_RPC_WRAPPERS
#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABasicGameGameModeBase(); \
	friend struct Z_Construct_UClass_ABasicGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ABasicGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BasicGame"), NO_API) \
	DECLARE_SERIALIZER(ABasicGameGameModeBase)


#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABasicGameGameModeBase(); \
	friend struct Z_Construct_UClass_ABasicGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ABasicGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BasicGame"), NO_API) \
	DECLARE_SERIALIZER(ABasicGameGameModeBase)


#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABasicGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABasicGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABasicGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABasicGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABasicGameGameModeBase(ABasicGameGameModeBase&&); \
	NO_API ABasicGameGameModeBase(const ABasicGameGameModeBase&); \
public:


#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABasicGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABasicGameGameModeBase(ABasicGameGameModeBase&&); \
	NO_API ABasicGameGameModeBase(const ABasicGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABasicGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABasicGameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABasicGameGameModeBase)


#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_12_PROLOG
#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_SPARSE_DATA \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_RPC_WRAPPERS \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_INCLASS \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_SPARSE_DATA \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	BasicGame_Source_BasicGame_BasicGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BASICGAME_API UClass* StaticClass<class ABasicGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BasicGame_Source_BasicGame_BasicGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
